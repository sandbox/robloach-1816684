// Initialize the lightbox.
Drupal.behaviors.jqcookielightbox = function (context) {
  // Check whether we are going to display the image.
  var image = Drupal.settings.jqcookie_lightbox.image || false;
  if (image) {
    // Retrieve the settings for the lightbox.
    var timeout = Drupal.settings.jqcookie_lightbox.timeout || 2000;
    var title = Drupal.settings.jqcookie_lightbox.title || '';
    var expire = Drupal.settings.jqcookie_lightbox.expire || 1;
    var cookiename = Drupal.settings.jqcookie_lightbox.cookie || 'jqcookie_lightbox';
    var rel = Drupal.settings.jqcookie_lightbox.rel || 'lightbox';

    // Set up the lightbox.
    $("body").append('<a href="' + image + '" rel="' + rel + '" id="jqcookie_lightbox" style="display:none" title="' + title + '">' + title + '</a>');

    // Wait for the timeout.
    setTimeout(function() {
      // Check the cookie to see if we are to display.
      var cookie = $.cookie(cookiename);
      if (!cookie) {
        // State that the lightbox has been displayed.
        $.cookie(cookiename, 'displayed', {
          'expires': expire
        });
        // Display the lightbox.
        $("#jqcookie_lightbox").click();
      }
    }, parseInt(timeout));
  }
};
